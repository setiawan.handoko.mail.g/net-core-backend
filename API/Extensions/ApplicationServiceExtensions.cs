using API.Data;
using API.Data.Implementations;
using API.Data.Repositories;
using API.Helpers;
using API.Tokenizer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace API.Extensions
{
    public static class ApplicationServiceExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config) {
            //service
            services.AddScoped<ITokenService, TokenServiceImpl>();
            //repository
            services.AddScoped<IEmailRepository, EmailRepositoryImpl>();
            //automapper
            services.AddAutoMapper(typeof(AutoMapperProfiles).Assembly);

            services.AddDbContext<DataContext>(options => {
                options.UseSqlite(config.GetConnectionString("DefaultConnection"));
            });

            return services;
        }
    }
}