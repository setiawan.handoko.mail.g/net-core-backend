using System.Collections.Generic;
using System.Threading.Tasks;
using API.Controllers.Base;
using API.Data;
using API.Data.Repositories;
using API.DTOs.EmailManagement;
using API.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers.Users
{
    public class EmailController : BaseApiController
    {
        private readonly IEmailRepository _repository;
        private readonly IMapper _mapper;

        public EmailController(IEmailRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EmailDto>>> GetEmails() {
            var emails = await _repository.GetAllEmailAsync();
            var emailsToResult = _mapper.Map<IEnumerable<EmailDto>>(emails);
            return Ok(emailsToResult);
        }

        [AllowAnonymous]
        [HttpGet("{email}")]
        public async Task<ActionResult<EmailDto>> GetEmailByEmailAddress(string email) {
            var eml = await _repository.GetEmailByEmailAddress(email);
            var emlToResult = _mapper.Map<EmailDto>(eml);
            return Ok(emlToResult);
        }
    }
}