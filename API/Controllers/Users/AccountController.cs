using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using API.Controllers.Base;
using API.Data;
using API.DTOs.AccountManagement;
using API.DTOs.UserManagement;
using API.Entities;
using API.Tokenizer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers.Users
{
    public class AccountController : BaseApiController
    {
        private readonly DataContext _context;
        private readonly ITokenService _tokenService;
        public AccountController(DataContext context, ITokenService tokenService)
        {
            _tokenService = tokenService;
            _context = context;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<ActionResult<UserDto>> Register([FromBody] RegisterDto registerDto)
        {

            if (await isUserExists(registerDto.Username))
            {
                return BadRequest("Username is already exists!");
            }

            var hmac = new HMACSHA512();
            var passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(registerDto.Password));
            var passwordSalt = hmac.Key;

            var user = new AppUser(registerDto.Username, passwordHash, passwordSalt, "default_name_" + registerDto.Username);

            _context.tblUsers.Add(user);
            await _context.SaveChangesAsync();

            //jwt user result with token
            var userReturn = new UserDto {
                Username = user.Username,
                Token = _tokenService.CreateToken(user),
                PasswordHash = user.PasswordHash,
                PasswordSalt = user.PasswordSalt,
                FullName = user.FullName
            };
            return userReturn;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<UserDto>> Login([FromBody] LoginDto loginDto)
        {
            var user = await _context.tblUsers
                .SingleOrDefaultAsync(x => x.Username.ToLower() == loginDto.Username.ToLower());

            if (user == null) return Unauthorized("Invalid Username!");

            var hmac = new HMACSHA512(user.PasswordSalt); //key
            var dbPassword = hmac.ComputeHash(Encoding.UTF8.GetBytes(loginDto.Password)); //db password hash

            for (int i = 0; i < dbPassword.Length; i++)
            {
                if (dbPassword[i] != user.PasswordHash[i]) return Unauthorized("Invalid Password!");
            }

            //jwt user result with token
            var userReturn = new UserDto {
                Username = user.Username,
                Token = _tokenService.CreateToken(user),
                PasswordHash = user.PasswordHash,
                PasswordSalt = user.PasswordSalt,
                FullName = user.FullName
            };
            return userReturn;
        }

        /* ---------- HELPERS ---------- */
        //is user exists ?
        private async Task<bool> isUserExists(string username)
        {
            return await _context.tblUsers.AnyAsync(x => x.Username.ToLower() == username.ToLower());
        }

    }
}