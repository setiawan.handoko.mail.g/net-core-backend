using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using API.Controllers.Base;
using API.Data;
using API.DTOs.EmailManagement;
using API.DTOs.UserManagement;
using API.Entities;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    public class UsersController : BaseApiController
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;

        public UsersController(DataContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [Authorize]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserDto>>> GetUsers() {
            var users = await _context.tblUsers.ToListAsync();
            var usersToReturn = _mapper.Map<IEnumerable<UserDto>>(users);
            return Ok(usersToReturn);
        }

        [Authorize]
        [HttpGet("byid/{id}")]
        public async Task<ActionResult<UserDto>> GetUserById(int id) {
            var user = await _context.tblUsers
                .Where(x => x.Id == id)
                .ProjectTo<UserDto>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
            return Ok(user);
        }

        [Authorize]
        [HttpGet("byusername/{username}")]
        public async Task<ActionResult<UserDto>> GetUserByUsername(string username) {
            var result = await _context.tblUsers
                .Where(x => x.Username == username)
                .ProjectTo<UserDto>(_mapper.ConfigurationProvider)
                .SingleOrDefaultAsync();
            return Ok(result);
        }

        [Authorize]
        [HttpPut]
        public async Task<ActionResult> UpdateUser([FromBody] UserUpdateDto userUpdateDto){
            var username = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            var user = await _context.tblUsers.SingleOrDefaultAsync(x => x.Username == username);
            
            _mapper.Map(userUpdateDto, user);

            _context.Update(user);

            if(await _context.SaveChangesAsync() > 0) return NoContent();

            return BadRequest("Failed to update user");
        }

        [Authorize]
        [HttpDelete("delete-data/{username}")]
        public async Task<ActionResult> DeleteData(string username){
            var user = await _context.tblUsers.FirstOrDefaultAsync(x => x.Username == username);
            if(user == null) return NotFound();

            _context.tblUsers.Remove(user);

            if(await _context.SaveChangesAsync() > 0) return NoContent();

            return BadRequest("Failed to delete user");

        }

    }
}