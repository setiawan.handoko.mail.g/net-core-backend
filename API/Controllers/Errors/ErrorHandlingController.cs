using System;
using System.Threading.Tasks;
using API.Controllers.Base;
using API.Data;
using API.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers.Errors
{
    public class ErrorHandlingController : BaseApiController
    {
        private readonly DataContext _context;

        public ErrorHandlingController(DataContext context)
        {
            _context = context;
        }

        [Authorize]
        [HttpGet("auth")]
        public ActionResult<string> GetSecretKey()
        {
            return "secret key";
        }

        [HttpGet("not-found")]
        public ActionResult<AppUser> GetNotFound()
        {
            var eCheck = _context.tblUsers.Find(-1);
            if (eCheck == null) return NotFound();
            return Ok(eCheck);
        }

        [HttpGet("server-error")]
        public ActionResult<string> GetServerError()
        {
            var eCheck = _context.tblUsers.Find(-1);
            var eCheckToReturn = eCheck.ToString();
            return eCheckToReturn;
        }

        [HttpGet("bad-request")]
        public ActionResult<string> GetBadRequest()
        {
            return BadRequest("This is Bad Request !");
        }


    }
}