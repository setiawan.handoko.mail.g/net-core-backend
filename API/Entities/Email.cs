using System.ComponentModel.DataAnnotations.Schema;

namespace API.Entities
{
    [Table("tblEmail")]
    public class Email
    {
        public int Id { get; set; }
        public string EmailAddress { get; set; }
        public string publicId { get; set; }
        public AppUser AppUser {get; set;}
        public int AppUserId {get; set;}
    }
}