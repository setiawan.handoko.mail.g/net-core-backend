using System.Collections.Generic;

namespace API.Entities
{
    public class AppUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string FullName {get; set; }
        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public ICollection<Email> Emails {get; set;}

        public AppUser() { }

        public AppUser(string username, byte[] passwordHash, byte[] passwordSalt)
        {
            this.Username = username;
            this.PasswordHash = passwordHash;
            this.PasswordSalt = passwordSalt;
        }

        public AppUser(string username, byte[] passwordHash, byte[] passwordSalt, string fullname){
            this.Username = username;
            this.PasswordHash = passwordHash;
            this.PasswordSalt = passwordSalt;
            this.FullName = fullname;
        }

    }
}