using System.Linq;
using API.DTOs.EmailManagement;
using API.DTOs.UserManagement;
using API.Entities;
using AutoMapper;

namespace API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<AppUser, UserDto>();
            CreateMap<Email, EmailDto>();

            CreateMap<UserUpdateDto, AppUser>();
        }
    }
}