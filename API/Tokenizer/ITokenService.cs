using API.Entities;

namespace API.Tokenizer
{
    public interface ITokenService
    {
        string CreateToken(AppUser user);
    }
}