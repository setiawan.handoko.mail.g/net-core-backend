using System.Collections.Generic;
using System.Threading.Tasks;
using API.Data.Repositories;
using API.Entities;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Implementations
{
    public class EmailRepositoryImpl : IEmailRepository
    {
        private readonly DataContext _context;

        public EmailRepositoryImpl(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Email>> GetAllEmailAsync()
        {
            return await _context.tblEmail.ToListAsync();
        }

        public async Task<Email> GetEmailByEmailAddress(string email)
        {
            return await _context.tblEmail.SingleOrDefaultAsync(x => x.EmailAddress == email);
        }

        public async Task<Email> GetEmailByIdAsync(int id)
        {
            return await _context.tblEmail.FindAsync(id);
        }

        public async Task<bool> SaveAllAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Update(Email email)
        {
            _context.Entry(email).State = EntityState.Modified;
        }
    }
}