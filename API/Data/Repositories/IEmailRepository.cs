using System.Collections.Generic;
using System.Threading.Tasks;
using API.Entities;

namespace API.Data.Repositories
{
    public interface IEmailRepository
    {
        //Inquiry Data
        Task<IEnumerable<Email>> GetAllEmailAsync();
        Task<Email> GetEmailByIdAsync(int id);
        Task<Email> GetEmailByEmailAddress(string email);

        //Actions
        void Update(Email email);
        Task<bool> SaveAllAsync();
       
    }
}