using API.DTOs.UserManagement;

namespace API.DTOs.EmailManagement
{
    public class EmailDto
    {
        
        public int Id { get; set; }
        public string EmailAddress { get; set; }
        public string publicId { get; set; }
        public int AppUserId {get; set;}
    }
}