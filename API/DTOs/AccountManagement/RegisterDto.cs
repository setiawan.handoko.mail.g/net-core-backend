using System.ComponentModel.DataAnnotations;

namespace API.DTOs.AccountManagement
{
    public class RegisterDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [StringLength(8, MinimumLength = 4)]
        public string Password { get; set; }
        public string FullName { get; set; }

        public RegisterDto() {}
        public RegisterDto(string username, string password){
            this.Username = username;
            this.Password = password;
        }
         public RegisterDto(string username, string password, string fullname){
            this.Username = username;
            this.Password = password;
            this.FullName = fullname;
        }
    }
}