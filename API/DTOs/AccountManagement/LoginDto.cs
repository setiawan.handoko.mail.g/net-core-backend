using System.ComponentModel.DataAnnotations;

namespace API.DTOs.AccountManagement
{
    public class LoginDto
    {
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }

        public LoginDto() {}
        public LoginDto(string username, string password){
            this.Username = username;
            this.Password = password;
        }
    }
}