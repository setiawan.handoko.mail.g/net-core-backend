using System.Collections.Generic;
using API.DTOs.EmailManagement;

namespace API.DTOs.UserManagement
{
    public class UserDto
    {
        public string Username { get; set; }
        public string Token { get; set; }

        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public string FullName { get; set;}
        public ICollection<EmailDto> Emails {get; set;} 
    }
}